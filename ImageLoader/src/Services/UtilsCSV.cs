﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using CsvHelper;
using ImageLoader.src.Interfaces;

namespace ImageLoader.src
{
    public class UtilsCSV: IFileUtils
    {
        public async Task<ICollection<string>> GetDataListAsync(string filePath)
        {
            var result = new HashSet<string>();

            using (TextReader fileReader = File.OpenText(filePath))
            using (var csv = new CsvReader(fileReader))
            {
                csv.Configuration.HasHeaderRecord = false;
                while (await csv.ReadAsync())
                {
                    for (var i = 0; csv.TryGetField<string>(i, out var value); i++)
                    {
                        result.Add(value);
                    }
                }
            }
            return result;
        }
    }
}
