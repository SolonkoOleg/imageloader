﻿using System;
using System.Threading.Tasks;

namespace ImageLoader.src.Helper.Interfaces
{
    public interface IRetryHelper
    {
        Task ExecuteAsync(Func<Task> action, int maxRetryCount, TimeSpan maxRetryDelay);
    }
}
