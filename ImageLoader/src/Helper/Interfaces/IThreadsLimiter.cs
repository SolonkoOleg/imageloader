﻿using System;
using System.Threading.Tasks;

namespace ImageLoader.src.Helper.Interfaces
{
    public interface IThreadsLimiter : IDisposable
    {
        Task WaitAsync();
        Task PerformActionAndReleaseAsync(Func<Task> action);
    }
}
