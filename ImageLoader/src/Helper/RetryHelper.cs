﻿using System;
using System.Threading.Tasks;
using ImageLoader.src.Helper.Interfaces;

namespace ImageLoader.src.Helper
{
    public class RetryHelper: IRetryHelper
    {
        public async Task ExecuteAsync(Func<Task> action, int maxRetryCount, TimeSpan maxRetryDelay)
        {
            var retryCount = 0;
            while (true)
            {
                try
                {
                    retryCount++;
                    await action();
                    break;
                }
                catch
                {
                    if (retryCount > maxRetryCount)
                        throw;
                }
                await Task.Delay(maxRetryDelay);
            }
        }
    }
}
