﻿using ImageLoader.src.Helper.Interfaces;

namespace ImageLoader.src.Helper
{
    internal class ThreadsLimiterFactory: IThreadsLimiterFactory
    {
        public virtual IThreadsLimiter Create(int initialCount)
        {
            return new ThreadsLimiter(initialCount);
        }

        public virtual IThreadsLimiter Create(int initialCount, int maxCount)
        {
            return new ThreadsLimiter(initialCount, maxCount);
        }
    }
}
