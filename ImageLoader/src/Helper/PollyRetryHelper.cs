﻿using System;
using System.Threading;
using System.Threading.Tasks;
using ImageLoader.src.Helper.Interfaces;
using Microsoft.Extensions.Logging;
using Polly;

namespace ImageLoader.src.Helper
{
    public class PollyRetryHelper: IRetryHelper
    {
        private readonly ILogger _logger;

        public PollyRetryHelper(ILogger<PollyRetryHelper> logger)
        {
            _logger = logger;
        }
        public async Task ExecuteAsync(Func<Task> action, int maxRetryCount, TimeSpan maxRetryDelay)
        {
            var retryPolicy = Policy
                .Handle<Exception>()
                .WaitAndRetryAsync(
                    maxRetryCount,
                    i => maxRetryDelay,
                    onRetry: OnReTryLogger);


            await retryPolicy.ExecuteAsync(action);
        }

        public void OnReTryLogger(Exception ex, TimeSpan timeSpan, int retryAttempt, Context context)
        {
            _logger.LogWarning($"Delay: {timeSpan.TotalSeconds}s, Retry №: {retryAttempt}, ThreadId: {Thread.CurrentThread.ManagedThreadId}");
        }
    }
}