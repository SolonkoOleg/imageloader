﻿using ImageLoader.src.Helper.Interfaces;
using ImageLoader.src.Interfaces;
using ImageLoader.src.Model;
using ImageLoader.src.Helper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Http;
using Microsoft.Extensions.Logging;
using Nito.AsyncEx;
using System;
using System.IO;

namespace ImageLoader.src
{
    internal class Program
    {
        private static IServiceProvider _serviceProvider;

        private static void Main()
        {
            _serviceProvider = ConfigureServices(GetImplementReTry());
            var imageLoader = _serviceProvider.GetService<IFileLoader>();
            AsyncContext.Run(() => imageLoader.DownloadRandomUrlsAsync(GetCountRequest()));
        }

        private static int GetCountRequest()
        {
            int countRequest;
            Console.Write("Number of requests to download data: ");
            var consoleCount = Console.ReadLine();
            while (!int.TryParse(consoleCount, out countRequest))
            {
                Console.Write("Not a valid number, try again: ");
                consoleCount = Console.ReadLine();
            }
            return countRequest;
        }

        private static ImplementReTry GetImplementReTry()
        {
            ImplementReTry implementReTry;
            Console.Write("Select ReTry(0-Polly; 1-Custom): ");
            var consoleLine = Console.ReadLine();
            while (!Enum.TryParse(consoleLine,out implementReTry) || 
                !Enum.IsDefined(typeof(ImplementReTry), implementReTry))
            {
                Console.Write("Not a valid number, try again: ");
                consoleLine = Console.ReadLine();
            }
            return implementReTry;
        }

        private static IServiceProvider ConfigureServices(ImplementReTry implementReTry)
        {
            var configuration = new ConfigurationBuilder()
              .SetBasePath(Directory.GetCurrentDirectory())
              .AddJsonFile("resources/app-settings.json", false)
              .Build();
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddOptions()
                .AddLogging(configure =>
                {
                    configure.AddConfiguration(configuration.GetSection("Logging"));
                    configure.AddConsole();
                })
                .Configure<AppSettings>(configuration.GetSection("Configuration"))
                .AddSingleton<IFileUtils, UtilsCSV>()
                .AddSingleton<IFileLoader, FileLoader>()
                .AddSingleton<IThreadsLimiterFactory, ThreadsLimiterFactory>();
            serviceCollection.AddHttpClient();
            serviceCollection = (ServiceCollection)ConfigureReTryHelper(serviceCollection, implementReTry);
            serviceCollection.RemoveAll<IHttpMessageHandlerBuilderFilter>();
            return serviceCollection.BuildServiceProvider();
        }

        private static IServiceCollection ConfigureReTryHelper(IServiceCollection serviceCollection, ImplementReTry implementReTry)
        {
            switch (implementReTry)
            {
                case ImplementReTry.Polly:
                    serviceCollection.AddTransient<IRetryHelper, PollyRetryHelper>();
                    break;
                case ImplementReTry.Custom:
                    serviceCollection.AddTransient<IRetryHelper, RetryHelper>();
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(implementReTry), implementReTry, null);
            }
            return serviceCollection;
        }
    }
}
