﻿using System.Threading.Tasks;

namespace ImageLoader.src.Interfaces
{
    internal interface IFileLoader
    {
        Task DownloadRandomUrlsAsync(int countRequest, int? maxDegreeOfParallelism = null);

        Task DownloadFileAsync(string url, string filePath);
    }
}
