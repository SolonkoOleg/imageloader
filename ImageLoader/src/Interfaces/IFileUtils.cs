﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImageLoader.src.Interfaces
{
    public interface IFileUtils
    {
        Task<ICollection<string>> GetDataListAsync(string filePath);
    }
}
