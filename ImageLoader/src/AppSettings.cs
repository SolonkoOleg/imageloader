﻿using System;

namespace ImageLoader.src
{
    public class AppSettings
    {
        public int BulkSize { get; set; }

        public string DownloadPath { get; set; }

        public string FileNameUrls { get; set; }

        public int MaxRetryCount { get; set; }

        public int DelaySecond { get; set; }

        public TimeSpan TimeSpanDelay => TimeSpan.FromSeconds(DelaySecond);
    }
}
